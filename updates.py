# AIGod by AIGod, GPL-3.0 license
import sys
from tkinter import *
import os
import requests
from bs4 import BeautifulSoup
import urllib
from tkinter.ttk import Progressbar
import tarfile
from sys import platform
from tkinter import messagebox
import ttkbootstrap as ttk
from ttkbootstrap.constants import *

dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))

# определение ос
def ups():
    global urls
    #url = 'https://www.mediafire.com/file/7yr25hxugnn207t/0ups.tar.gz'
    if platform == "linux" or platform == "linux2":
        pars(urls[0]) # url test
    elif platform == "darwin":
        pass
    elif platform == "win32":
        pars(urls[1])

# парсер ссылки на истинную
def pars(url):
    global urlleng
    filename = 'update.tar.gz'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'lxml')
    links = soup.find_all('a', id='downloadButton')

    for link in links:
        hrefs = link.get('href')
        d = requests.get(hrefs)
        urlleng = int(d.headers['Content-Length'])
        urllib.request.urlretrieve(hrefs, filename, show_progress)

# распаковка файла
def filesupd():
    tar = tarfile.open(dir+'/update.tar.gz')
    tar.extractall()
    tar.close()
    try:
        os.remove(dir+'/update.tar.gz')
    except:
        pass
    infos()

# INFO
def infos():
    messagebox.showinfo('About AIGod', 'Finish')
    sys.exit()

# прогресс бар скачивания
def show_progress(block_num, block_size, total_size):
    global urlleng
    bar.config(maximum=urlleng)

    downloaded = block_num * block_size

    if downloaded < urlleng:
        bar['value'] = downloaded
        bar.grid(row=1, column=0, sticky=W+E, padx=(5, 5), pady=(5, 5))
    else:
        filesupd()

rf = open(dir + '/option.dat', 'r')
oplist = rf.readlines()
rf.close()

# проверка темы
try:
    theme_stand = str(''.join(oplist[1].split('\n', 1)))
except IOError:
    theme_stand = 'cosmo'

window = ttk.Window(title="AIGod", themename=theme_stand, iconphoto=dir + "/bin/image/logo.png",
                    resizable=[0, 0]) # size=[500, 100]

versions = '2.3' # AIGod v№ # Версия компиляции
urls = []
urlleng = 0

english = ['You’re using the latest version.', 'Update', 'Error: Check your internet connection',
           'Close the master program AIGod'] #0-3
russian = ['Вы используете последнюю версию', 'Обновить', 'Ошибка: Проверьте ваше интернет соединение',
           'Закройте основную программу AIGod']
ukraine = ["Ви використовуєте останню версію", "Оновити", "Помилка: Перевірте підключення до Інтернету",
           "Закрийте основну програму AIGod"]
spanish = ['Usas la última versión', 'Actualizar', 'Error: verifique su conexión a Internet',
           'Cierre el programa principal de AIGod']
italian = ["Usi l'ultima versione", 'Aggiornare', 'Errore: controlla la tua connessione Internet',
           'Chiudi il programma AIGod principale']
german = ['Sie verwenden die neueste Version', 'Aktualisieren', 'Fehler: Überprüfen Sie Ihre Internetverbindung',
          'Schließen Sie das Haupt AIGod Programm']
norwegian = ['Du bruker den nyeste versjonen', 'Oppdater', 'Feil: Sjekk internettforbindelsen din',
             'Lukk det viktigste AIGod programmet']
french = ['Vous utilisez la dernière version', 'Mise à jour', 'Erreur: vérifiez votre connexion Internet',
          'Fermez le principal programme AIGod']

# проверка настройки языка
try:
    str_lang_next = str(''.join(oplist[0].split('\n', 1)))
    if str_lang_next == 'en':
        lang = english
    elif str_lang_next == 'ru':
        lang = russian
    elif str_lang_next == 'ua':
        lang = ukraine
    elif str_lang_next == 'sp':
        lang = spanish
    elif str_lang_next == 'it':
        lang = italian
    elif str_lang_next == 'gr':
        lang = german
    elif str_lang_next == 'nr':
        lang = norwegian
    elif str_lang_next == 'fr':
        lang = french
except IOError:
    str_lang_next = "en"
    lang = english

# лейблы, инпуты, кнопки
lbl = Label(window, text=lang[0])
lbl2 = Label(window, text=lang[3])
btn = Button(window, text=lang[1], command=ups)
bar = Progressbar(window) #length=280
bar['value'] = 0

# Парсинг сайта
try:
    url = 'https://codeberg.org/krolaper/aigod/releases'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'lxml')
    items = soup.find('ul', id='release-list')
    vers = items.find('h4')
    textver = vers.find('a').text

    textver = str(''.join(textver.split('v', 1)[1]))

    if float(textver) > float(versions):
        pages = items.find('div', class_='markup desc')
        links = pages.find_all('a')

        for link in links:
            hrefval = link.get('href')
            urls.append(hrefval)
        btn.config(text=lang[1], command=ups)
        btn.grid(row=0, column=0, padx=(5,5), pady=(5,5))
        bar.grid(row=1, column=0, sticky=W+E, padx=(5, 5), pady=(5, 5))
        lbl2.config(text=lang[3])
        lbl2.grid(row=2, column=0, padx=(5,5), pady=(5,5))
    else:
        lbl.config(text=lang[0])
        lbl.grid(row=0, column=0, padx=(5,5), pady=(5,5))
except:
    lbl.config(text=lang[2])
    lbl.grid(row=0, column=0, padx=(5,5), pady=(5,5))

# Отрисовка окна
window.mainloop()