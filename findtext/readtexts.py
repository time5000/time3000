# AIGod by AIGod, GPL-3.0 license
import easyocr #распознание текста на фото
import os
from tkinter import *
from tkinter.ttk import Progressbar
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
import shutil
from findtext.readres import textresult

dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))
pth = dir.replace('findtext', '')

list_res = []

def text_recognition():
    global list_res
    list_res = []
    rf = open(pth + 'optionmodel.dat', 'r')
    rrr = rf.readlines()
    u1 = rrr[3]
    cr1 = rrr[4]
    anal = rrr[11]
    rf.close()
    u1 = str(''.join(u1.split('\n')[:1]))
    cr1 = str(''.join(cr1.split('\n')[:1]))
    anal = str(''.join(anal.split('\n')[:1]))

    try:
        ff = open(pth + 'option.dat', 'r')
        oplist = ff.readlines()
        ff.close()
    except IOError:
        oplist = ['en', 'cosmo']

    # проверка темы
    try:
        theme_stand = str(''.join(oplist[1].split('\n', 1)))
    except IOError:
        theme_stand = 'cosmo'
    window1 = ttk.Toplevel(title="AIGod", iconphoto=pth + "bin/image/logo.png",
                           resizable=[0, 0])
    ttk.Style().theme_use(theme_stand)

    # лейблы, инпуты, кнопки
    lbl = Label(window1, text='Comparison: 0/0')
    bar = Progressbar(window1, maximum=0)
    bar['value'] = 0
    bar.grid(row=0, column=0, sticky=W + E, padx=(5, 5), pady=(5, 5))
    window1.update()

    arrRes = None
    pathRes = pth + 'findface/esrgan/results/'

    for dirs1,folder1,files1 in os.walk(pathRes):
        arrRes = files1

    ff = open(pth + 'find.dat', 'r')
    olist = ff.readlines()
    ff.close()

    num, maxx = 0, 0
    maxx = len(arrRes)
    for i in arrRes:
        file_path = pathRes + i # путь к проверяемому фото
        #reader = easyocr.Reader(['ab', 'ady', 'af', 'am', 'ar', 'az', 'be', 'bg', 'bn', 'bs', 'ch-pin-syl', 'ch_pin', 'cs', 'cy', 'da', 'de', 'en', 'es', 'et', 'fa', 'fr', 'ga', 'ge', 'gre', 'he', 'hi', 'hr', 'hu', 'id', 'is', 'it', 'ja', 'kn', 'ko', 'ku', 'la', 'lt', 'lv', 'mi', 'ml', 'mn', 'mr', 'ms', 'mt', 'my', 'ne', 'nl', 'no', 'oc', 'or', 'pb', 'pi', 'pl', 'pt', 'ro', 'rs_cyrillic', 'rs_latin', 'ru', 'sh', 'sk', 'sl', 'sq', 'sv', 'sw', 'ta', 'te', 'th', 'ti', 'ti2', 'tjk', 'tl', 'tr', 'ug', 'uk', 'ur', 'uz', 'vi'])
        reader = easyocr.Reader(['en'])
        result = reader.readtext(file_path, detail=0, paragraph=True) #paragraph(групирует текст)

        if len(result) != 0:
            for line in result:
                analife = False
                if u1 == 'url' or u1 == 'web':
                    if cr1 == 'true' and anal == 'true':
                        analife = True

                if analife == True:
                    try:
                        shutil.copy(pathRes + i, pth + 'findface/esrgan/objects/') # 2 вконце +i
                        list_res.append(pth + 'findface/esrgan/objects/'+i + '=====' + line)
                        wrs = open(pth + 'resfind.dat', 'a')
                        wrs.write(pth + 'findface/esrgan/objects/'+i + '=====' + line + '\n')
                        wrs.close()
                    except OSError:
                        pass
                else:
                    #olist[num] = str(''.join(olist[num].split('\n')[:1]))
                    #list_res.append(olist[num] + '=====' + line)
                    list_res.append(pathRes + i + '=====' + line)

                    wrs = open(pth + 'resfind.dat', 'a')
                    #wrs.write(olist[num] + '=====' + line + '\n')
                    wrs.write(pathRes + i + '=====' + line + '\n')
                    wrs.close()

                num += 1
        else:
            print('no number')
            olist[num] = str(''.join(olist[num].split('\n')[:1]))
            try:
                os.remove(pathRes + i)  # удаление проверяемую фотографии
                if u1 == 'url' or u1 == 'web':
                    if cr1 == 'true' and anal == 'true':
                        try:
                            os.remove(olist[num])  # удаление проверяемую фотографии оригинал
                        except:
                            pass
            except OSError:
                print('Error: ' + pathRes + i + '\nError: ' + olist[num])
            num += 1

        lbl.config(text='Comparison: ' + str(num) + '/' + str(maxx))
        # прогрессбар
        bar.config(maximum=maxx)
        bar['value'] = num
        bar.grid(row=0, column=0, sticky=W + E, padx=(5, 5), pady=(5, 5))
        lbl.grid(row=1, column=0, padx=(5, 5), pady=(5, 5))
        window1.update()
        if num >= maxx:
            window1.destroy()

        if len(list_res) != 0:
            textresult()
        else:
            wropt = open(pth + 'analizy.dat', 'a')
            wropt.write("\nfalse")
            wropt.close()