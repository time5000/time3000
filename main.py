# AIGod by AIGod, GPL-3.0 license
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter.ttk import Combobox
from tkinter import scrolledtext
from tkinter.ttk import Checkbutton
from tkinter.ttk import Style
from tkinter.ttk import Notebook
from PIL import Image, ImageTk
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
import time
import sys
import os
import shutil

from yolov5.detect import mains
from yolov5.resultimg import imgmain
from yolov5.resultvideo import vidmain
import pygame
from sys import platform
import subprocess
import hashlib
from findface.esrgan.mainf import mainf as esrmain
import urllib.request as urllib2
from wiki.wiki import switch
from yolov5.opens import look

#sys.setrecursionlimit(10000)
dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))

# кнопка выбора файла
def pathing1():
    txt.delete(0, END)
    if selected.get() == 1:
        paths1 = filedialog.askopenfilename(filetypes = (("Image files", ["*.jpg", "*.jpeg", "*.png", "*.bmp", "*.tif",
                                                                         "*.tiff", "*.heic", "*.dng", "*.mpo", "*.webp",
                                                                         "*.pfm", "*.JPG", "*.JPEG", "*.PNG", "*.BMP",
                                                                         "*.TIF", "*.TIFF", "*.HEIC", "*.DNG", "*.MPO",
                                                                         "*.WEBP", "*.PFM"]),("all files", "*.*"))) # ,("all files", "*.*")
    elif selected.get() == 2:
        paths1 = filedialog.askopenfilename(filetypes = (("Video files", ["*.mp4", "*.mov", "*.asf", "*.gif", "*.avi",
                                                                        "*.m4v", "*.mkv", "*.mpeg", "*.mpg", "*.ts",
                                                                        "*.wmv", "*.MP4", "*.MOV", "*.ASF", "*.GIF",
                                                                        "*.AVI", "*.M4V", "*.MKV", "*.MPEG", "*.MPG",
                                                                        "*.TS", "*.WMV"]),("all files", "*.*"))) # ,("all files", "*.*")
    else:
        paths1 = filedialog.askopenfilename(filetypes = (("all files", "*.*")))
    global namefile
    global liner
    txt.insert(0, paths1)
    pathtxt = txt.get()
    if pathtxt != '':
        namefile = ''.join(paths1.split('/')[-1:])
        liner = pathtxt
        btns.config(command=startdetect)
        btns.grid(row=25, column=2, sticky=E, padx=(5, 5), pady=(5, 5))
    else:
        namefile = ''
        liner = ''
        btns.grid_forget()

# кнопка выбора каталога с файлами
def pathing2():
    txt.delete(0, END)
    paths2 = filedialog.askdirectory()
    txt.insert(0, paths2)
    pathtxt = txt.get()
    global liner
    if pathtxt != '':
        liner = pathtxt
        btns.config(command=startdetect)
        btns.grid(row=25, column=2, sticky=E, padx=(5, 5), pady=(5, 5))
    else:
        liner = ''
        btns.grid_forget()

# кнопка выбора файла модели
def pathing3():
    txt2.delete(0, END)
    paths3 = filedialog.askopenfilename(filetypes = (("Model files", ["*.pt"]),("all files", "*.*")))
    txt2.insert(0, paths3)
    pathtxt = txt2.get()
    global model
    if pathtxt != '':
        model = pathtxt
        btns.grid(row=25, column=2, sticky=E, padx=(5, 5), pady=(5, 5))
    else:
        model = dir+"/yolov5/modres/Objects365.pt"
        txt2.delete(0, END)
        txt2.insert(0, model)

# кнопка выбора каталога с базой tab2
def pathing4():
    txtbase.delete(0, END)
    paths4 = filedialog.askdirectory()
    txtbase.insert(0, paths4)
    pathtxtbase = txtbase.get()
    global bases
    if pathtxtbase != '':
        bases = pathtxtbase
        btns2.grid(row=25, column=2, sticky=E, padx=(5, 5), pady=(5, 5))
    else:
        bases = ''
        btns2.grid_forget()

# кнопка выбора каталога с лицами tab2
def pathing5():
    txtfoto.delete(0, END)
    paths5 = filedialog.askdirectory()
    txtfoto.insert(0, paths5)
    pathtxtfoto = txtfoto.get()
    global fotos
    if pathtxtfoto != '':
        fotos = pathtxtfoto
        btns2.grid(row=25, column=2, sticky=E, padx=(5, 5), pady=(5, 5))
    else:
        fotos = ''
        btns2.grid_forget()

# кнопка выбора каталога с базой tab1
def pathing6():
    txtbaseA.delete(0, END)
    paths6 = filedialog.askdirectory()
    txtbaseA.insert(0, paths6)
    pathtxtbase = txtbaseA.get()
    global bases
    if pathtxtbase != '':
        bases = pathtxtbase
    else:
        bases = ''

# кнопка запуска ссылки
def startweb():
    pathtxt = txt.get()
    global liner
    if pathtxt != '':
        liner = pathtxt
        startdetect()
    else:
        liner = ''
        messagebox.showinfo(lang[38], lang[41])

# кнопка запуска вебкамеры
def startcam():
    pathtxt = txt.get()
    global liner
    if pathtxt != '':
        wrfile = open(dir+'/manyCam.txt', 'w')
        wrfile.write('')
        wrfile.close()
        if pathtxt.find(",") >= 0:
            pathtxt = pathtxt.replace(',', '\n')
            pathtxt = ''.join(i for i in pathtxt if not i.isalpha())
            wrfile = open(dir+'/manyCam.txt', 'w')
            wrfile.write(pathtxt)
            wrfile.close()
            liner = dir+'/manyCam.txt'
        else:
            liner = pathtxt
        startdetect()
    else:
        liner = ''
        messagebox.showinfo(lang[38], lang[74])

# Список формата запроса
def lookupCustomer(event):
    if combo.current() == 0:
        btns.grid_forget()
        chk1.grid_forget()
        txt.delete(0, END)
        lbl.config(text=lang[42])
        btn.config(text=lang[32], command=pathing1)
        comboobj.grid(row=0, column=2, sticky=W, padx=(5,5), pady=(5,5))
        btn.grid(row=1, column=2, sticky=W + NS, padx=(5, 5), pady=(5, 5))
    elif combo.current() == 1:
        btns.grid_forget()
        chk1.grid_forget()
        txt.delete(0, END)
        lbl.config(text=lang[43])
        btn.config(text=lang[32], command=pathing2)
        comboobj.grid(row=0, column=2, sticky=W, padx=(5,5), pady=(5,5))
        btn.grid(row=1, column=2, sticky=W + NS, padx=(5, 5), pady=(5, 5))
    elif combo.current() == 2:
        btns.grid_forget()
        comboobj.grid_forget()
        btn.grid_forget()
        txt.delete(0, END)
        lbl.config(text=lang[44])
        #btn.config(text=lang[33], command=startweb)
        btns.config(command=startweb)
        btns.grid(row=25, column=2, sticky=E, padx=(5, 5), pady=(5, 5))
        chk1.grid(row=6, column=0, sticky=W, padx=(5,5), pady=(5,5))
    elif combo.current() == 3:
        btns.grid_forget()
        comboobj.grid_forget()
        chk1.grid_forget()
        btn.grid_forget()
        txt.delete(0, END)
        txt.insert(0, "0")
        lbl.config(text=lang[73])
        #btn.config(text=lang[33], command=startcam)
        btns.config(command=startcam)
        btns.grid(row=25, column=2, sticky=E, padx=(5, 5), pady=(5, 5))

# Список моделей
def lookupCustomer2(event):
    global model
    if combomodel.current() == 0:
        txt2.grid_forget()
        btn2.grid_forget()
        chk3.grid_forget()
        lblbaseA.grid_forget()
        txtbaseA.grid_forget()
        btnbaseA.grid_forget()
        combocat.grid(row=2, column=2, sticky=W, padx=(5,5), pady=(5,5))
        txtscroll.config(state='normal')
        txtscroll.delete(1.0, END)
        txtobj.delete(0, END)
        txtscroll.insert(INSERT, lang[51])
        txtscroll.config(state='disabled')
        model = dir+"/yolov5/modres/Objects365.pt"
    elif combomodel.current() == 1:
        combocat.grid_forget()
        txt2.grid_forget()
        btn2.grid_forget()
        chk3.grid_forget()
        lblbaseA.grid_forget()
        txtbaseA.grid_forget()
        btnbaseA.grid_forget()
        txtscroll.config(state='normal')
        txtscroll.delete(1.0, END)
        txtobj.delete(0, END)
        txtscroll.insert(INSERT, lang[48])
        txtscroll.config(state='disabled')
        model = dir+"/yolov5/modres/AerialQvadracopt.pt"
    elif combomodel.current() == 2:
        combocat.grid_forget()
        txt2.grid_forget()
        btn2.grid_forget()
        chk3.grid_forget()
        lblbaseA.grid_forget()
        txtbaseA.grid_forget()
        btnbaseA.grid_forget()
        txtscroll.config(state='normal')
        txtscroll.delete(1.0, END)
        txtobj.delete(0, END)
        txtscroll.insert(INSERT, lang[50])
        txtscroll.config(state='disabled')
        model = dir+"/yolov5/modres/Aquarium.pt"
    elif combomodel.current() == 3:
        combocat.grid_forget()
        txt2.grid_forget()
        btn2.grid_forget()
        chk3.grid_forget()
        lblbaseA.grid_forget()
        txtbaseA.grid_forget()
        btnbaseA.grid_forget()
        txtscroll.config(state='normal')
        txtscroll.delete(1.0, END)
        txtobj.delete(0, END)
        txtscroll.insert(INSERT, lang[69])
        txtscroll.config(state='disabled')
        model = dir+"/yolov5/modres/military1.pt"
    elif combomodel.current() == 4:
        combocat.grid_forget()
        txt2.grid_forget()
        btn2.grid_forget()
        chk3.grid_forget()
        lblbaseA.grid_forget()
        txtbaseA.grid_forget()
        btnbaseA.grid_forget()
        txtscroll.config(state='normal')
        txtscroll.delete(1.0, END)
        txtobj.delete(0, END)
        txtscroll.insert(INSERT, lang[72])
        txtscroll.config(state='disabled')
        model = dir+"/yolov5/modres/WildfireSmoke.pt"
    elif combomodel.current() == 5:
        combocat.grid_forget()
        txt2.grid_forget()
        btn2.grid_forget()
        txtscroll.config(state='normal')
        txtscroll.delete(1.0, END)
        txtobj.delete(0, END)
        txtscroll.insert(INSERT, lang[76])
        txtscroll.config(state='disabled')
        model = dir+"/yolov5/modres/facedetect.pt"
    elif combomodel.current() == 6:
        combocat.grid_forget()
        txt2.grid_forget()
        btn2.grid_forget()
        txtscroll.config(state='normal')
        txtscroll.delete(1.0, END)
        txtobj.delete(0, END)
        txtscroll.insert(INSERT, lang[104])
        txtscroll.config(state='disabled')
        model = dir+"/yolov5/modres/license_plate.pt"
    elif combomodel.current() == 7:
        combocat.grid_forget()
        chk3.grid_forget()
        lblbaseA.grid_forget()
        txtbaseA.grid_forget()
        btnbaseA.grid_forget()
        txtscroll.config(state='normal')
        txtscroll.delete(1.0, END)
        txtobj.delete(0, END)
        txtscroll.config(state='disabled')
        txt2.grid(row=3, column=1, sticky=W + E, padx=(5, 5), pady=(5, 5))
        btn2.grid(row=3, column=2, sticky=W, padx=(5, 5), pady=(5, 5))

# Список объектов
def lookupCustomer3(event):
    txtscroll.config(state='normal')
    txtscroll.delete(1.0, END)
    if combocat.current() == 0:
        txtscroll.insert(INSERT, lang[51])
    elif combocat.current() == 1:
        txtscroll.insert(INSERT, lang[52])
    elif combocat.current() == 2:
        txtscroll.insert(INSERT, lang[53])
    elif combocat.current() == 3:
        txtscroll.insert(INSERT, lang[54])
    elif combocat.current() == 4:
        txtscroll.insert(INSERT, lang[55])
    elif combocat.current() == 5:
        txtscroll.insert(INSERT, lang[56])
    elif combocat.current() == 6:
        txtscroll.insert(INSERT, lang[57])
    elif combocat.current() == 7:
        txtscroll.insert(INSERT, lang[58])
    elif combocat.current() == 8:
        txtscroll.insert(INSERT, lang[59])
    elif combocat.current() == 9:
        txtscroll.insert(INSERT, lang[60])
    elif combocat.current() == 10:
        txtscroll.insert(INSERT, lang[61])
    elif combocat.current() == 11:
        txtscroll.insert(INSERT, lang[62])
    elif combocat.current() == 12:
        txtscroll.insert(INSERT, lang[63])
    elif combocat.current() == 13:
        txtscroll.insert(INSERT, lang[64])
    elif combocat.current() == 14:
        txtscroll.insert(INSERT, lang[65])
    elif combocat.current() == 15:
        txtscroll.insert(INSERT, lang[66])
    txtscroll.config(state='disabled')

# Выбор фото/видео
def lookupCustomer1(event):
    if comboobj.current() == 0:
        selected.set(1)
    elif comboobj.current() == 1:
        selected.set(2)

# Выбор разрешение экрана результата
def lookupCustomer4(event):
    global mw, mh
    sz = combosize.get()
    mw = ''.join(sz.split('.')[0]) #:-1
    mh = ''.join(sz.split('.')[1])

# проверка результата
def checktimeres():
    f = open(dir + '/pathlogfile.dat', 'r')
    ip = f.readlines()
    f.close()
    respath = str(''.join(ip[0].split('\n')[:1]))
    if respath != '':
        if combo.current() == 0:
            pathss = open(dir + '/pathlogfile.dat', 'w')
            pathss.write(str(respath + '/' + namefile))
            pathss.close()
            lblres.config(text=lang[97])
            txtres.insert(0, str(dir + '/' + respath + '/' + namefile))
            lblres.grid(row=25, column=0, sticky=W + NS, padx=(5, 5), pady=(5, 5))
            txtres.grid(row=25, column=1, sticky=W + E, padx=(5, 5), pady=(5, 5))
            if openRes == 'unic':
                look(cl=False)
            elif openRes == 'default':
                if selected.get() == 1:
                    imgmain(mw, mh)
                elif selected.get() == 2:
                    vidmain(mw, mh)
        elif combo.current() == 1:
            pathres = []
            for dirs1, folder1, files1 in os.walk(str(respath) + '/'):
                arrRes = files1

            if selected.get() == 1:
                tplist = ["*.jpg", "*.jpeg", "*.png", "*.bmp", "*.tif", "*.tiff", "*.heic", "*.dng", "*.mpo", "*.webp",
                          "*.pfm", "*.JPG", "*.JPEG", "*.PNG", "*.BMP", "*.TIF", "*.TIFF", "*.HEIC", "*.DNG", "*.MPO",
                          "*.WEBP", "*.PFM"]
            elif selected.get() == 2:
                tplist = ["*.mp4", "*.mov", "*.asf", "*.gif", "*.avi", "*.m4v", "*.mkv", "*.mpeg", "*.mpg", "*.ts",
                          "*.wmv", "*.MP4", "*.MOV", "*.ASF", "*.GIF", "*.AVI", "*.M4V", "*.MKV", "*.MPEG", "*.MPG",
                          "*.TS", "*.WMV"]
            for fls in arrRes:
                for tps in tplist:
                    if fls.find(tps) > -1:
                        pathres.append(fls)
                    else:
                        pass

            lblres.config(text=lang[97])
            txtres.insert(0, str(dir + '/' + respath + '/'))
            lblres.grid(row=25, column=0, sticky=W + NS, padx=(5, 5), pady=(5, 5))
            txtres.grid(row=25, column=1, sticky=W + E, padx=(5, 5), pady=(5, 5))

            for namefiles in pathres:
                pathss = open(dir + '/pathlogfile.dat', 'a')
                pathss.write(str(respath + '/' + namefiles) + '\n')
                pathss.close()
                if openRes == 'unic':
                    look(cl=False)
                elif openRes == 'default':
                    if selected.get() == 1:
                        imgmain(mw, mh)
                    elif selected.get() == 2:
                        vidmain(mw, mh)
        else:
            time.sleep(15)
            checktimeres()

# проверка состояния интернета
def internet_on():
    for timeout in [1,5,10,15,30,50,100,150]:
        try:
            response=urllib2.urlopen('https://codeberg.org/krolaper/aigod',timeout=timeout)
            return True
        except Exception as ex:
            #print(ex)
            pass
    return False

# кнопка запуска
def startdetect():
    lblres.grid_forget()
    txtres.grid_forget()

    clearFile = ['/pathlogfile.dat', '/optionmodel.dat', '/find.dat']
    for clf in clearFile:
        wrfile = open(dir + clf, 'w')
        wrfile.write('')
        wrfile.close()

    delNameFile = ['/resfind.dat', '/analizy.dat', '/objectsText.dat', '/objects.dat']
    for dell in delNameFile:
        try:
            os.remove(dir + dell)
        except OSError:
            pass

    if combo.current() == 0:
        fn = check_sha(hash_factory=hashlib.md5, chunk_num_blocks=128)

        pathss = open(dir + '/pathlogfile.dat', 'w')
        pathss.write(fn)
        pathss.close()
    else:
        fn = ''

    global classs, url
    objnum = txtobj.get()

    if objnum == '':
        classs = objnum
    elif objnum != '':
        objnum = objnum.replace(' ', '')
        objnum = ''.join(i for i in objnum if not i.isalpha())
        classs = objnum

    if combo.current() == 2:
        url = "url"
    elif combo.current() == 3:
        url = "web"
    else:
        url = ""

    if cut_state.get() == True:
        crop = 'true'
    else:
        crop = 'false'

    if combo.current() == 2 and detect_state.get() == True:
        det_chk = 'true'
    else:
        det_chk = 'false'

    if wiki_state.get() == True:
        wiks = 'true'
    else:
        wiks = 'false'

    if anal_state.get() == True:
        anal = 'true'
    else:
        anal = 'false'

    wropt = open(dir+'/optionmodel.dat', 'w')  # парамметры модели
    # модель + #дата + путь к файлу + класс + если ссылка + вырез картики рзульт + уведомление
    # + размер окна результата (длина + высота) + хеш с историей + wiki имен детекта + база фотоко
    # + анализ фото #0-11
    wropt.write(model + "\n" + liner + "\n" + classs + "\n" + url + "\n" + crop + "\n" + det_chk +
                "\n" + str(mw) + "\n" + str(mh) + "\n" + newCheck + "\n" + wiks + "\n" + bases +
                "\n" + anal + "\n") # dataset
    wropt.close()

    if combo.current() == 2 or combo.current() == 3:
        selected.set(2)

    if combo.current() == 2:
        inet = internet_on()

    if liner == '':
        messagebox.showinfo(lang[38], lang[39]+'\nor\n'+lang[40])
    elif model =='':
        messagebox.showinfo(lang[38], lang[39])
    elif combo.current() == 2 and inet == False:
        messagebox.showinfo(lang[38], lang[98])
    else:
        if selected.get() == 1 or selected.get() == 2:
            if fn == '':
                mains()
                pass
            checktimeres()
        else:
            messagebox.showinfo(lang[38], lang[67])

        if anal_state.get() == True:
            if combomodel.current() == 5 or combomodel.current() == 6:
                fi = open(dir + '/pathlogfile.dat', 'r')
                ips = fi.readlines()
                fi.close()
                if ips != '':
                    ips[0] = str(''.join(ips[0].split('\n')[:1]))
                    fotopath = str('/'.join(ips[0].split("/")[:-1]))
                    if combomodel.current() == 5:
                        pths = dir + '/' + fotopath + '/crops/face/'
                    elif combomodel.current() == 6:
                        pths = dir + '/' + fotopath + '/crops/licence/'
                    fotos = pths

                    wropt = open(dir + '/analizy.dat', 'w')  # парамметры анализа
                    # вариант анализа + путь к базе + путь к фото с лицами #0-2 3-результат
                    if combomodel.current() == 5:
                        wropt.write(varss + '\n' + fotos + '\n' + bases + '\n')
                    if combomodel.current() == 6:
                        wropt.write(varss + '\n' + fotos + '\n')
                    wropt.close()

                    if combomodel.current() == 5:
                        esrmain(0)
                    if combomodel.current() == 6:
                        esrmain(1)

        if wiki_state.get() == True:
            # проверка существования файла
            if os.path.exists(dir + '/objects.dat') == True:
                pass
            else:
                pathss = open(dir + '/objects.dat', 'w')  # сохранение директории в файл
                pathss.write('')
                pathss.close()

            switch(str_lang_next)

            kk = open(dir + '/objectsText.dat', 'r')
            hh = kk.readlines()
            kk.close()

            wikiscroll.insert(INSERT, hh)
            tab_control.select(tab2)

# Чек хеш файла
def check_sha(hash_factory=hashlib.md5, chunk_num_blocks=128):
    lblres.grid_forget()
    txtres.grid_forget()
    filename = liner
    h = hash_factory()
    with open(filename, 'rb') as f:
        while chunk := f.read(chunk_num_blocks * h.block_size):
            h.update(chunk)
    res = h.digest() # результат хеш файла

    # проверка по истории
    clearFile = ['/pathlogfile.dat', '/optionmodel.dat', '/find.dat']
    for clf in clearFile:
        wrfile = open(dir + clf, 'w')
        wrfile.write('')
        wrfile.close()

    delNameFile = ['/resfind.dat', '/analizy.dat', '/objectsText.dat', '/objects.dat']
    for dell in delNameFile:
        try:
            os.remove(dir + dell)
        except OSError:
            pass

    global classs, newCheck
    objnum = txtobj.get()
    if objnum == '':
        classs = objnum
    elif objnum != '':
        objnum = objnum.replace(' ', '')
        objnum = ''.join(i for i in objnum if not i.isalpha())
        classs = objnum
    newCheck = str(res) +"*****"+ model +"#####"+ liner +"+++++"+ classs
    fin = ''
    with open(dir+'/history.dat', 'r') as f:
        for line in f:
            lines = str(''.join(line.split('\n')[:1]))
            lines = str(''.join(lines.split("=====")[0]))
            if newCheck in lines:
                line = str(''.join(line.split('\n')[:1]))
                line = str(''.join(line.split('=====', 1)[1]))
                fin = line
                break
    return fin

# кнопка запуска2
def start2():
    lblresf.grid_forget()

    wrs = open(dir + '/find.dat', 'w')
    wrs.write("")
    wrs.close()

    wropt = open(dir + '/analizy.dat', 'w')  # парамметры анализа
    # вариант анализа + путь к базе + путь к фото с лицами #0-2 3-результат
    wropt.write(varss + '\n' + fotos + '\n' + bases + '\n')
    wropt.close()

    if combovar.current() == 0:
        for dirs1, folder1, files1 in os.walk(bases):
            arr1 = files1
        for dirs2, folder2, files2 in os.walk(fotos):
            arr2 = files2

        if arr1 != '' and arr2 != '':
            esrmain(varss)

            rr = open(dir + '/analizy.dat', 'r')
            ph = rr.readlines()
            rr.close()
            ph[3] = str(''.join(ph[3].split('\n')[:1]))
            if ph[3] == 'false':
                lblresf.config(text=lang[94])
                lblresf.grid(row=25, column=0, sticky=W + NS, padx=(5, 5), pady=(5, 5))
        else:
            messagebox.showinfo(lang[38], lang[90])
    elif combovar.current() == 1:
        for dirs3, folder3, files3 in os.walk(fotos):
            arr3 = files3

        if arr3 != '':
            esrmain(varss)

            rr = open(dir + '/analizy.dat', 'r')
            ph = rr.readlines()
            rr.close()
            ph[2] = str(''.join(ph[2].split('\n')[:1]))
            if ph[2] == 'false':
                lblresf.config(text=lang[94])
                lblresf.grid(row=25, column=0, sticky=W + NS, padx=(5, 5), pady=(5, 5))
        else:
            messagebox.showinfo(lang[38], lang[90])

# варианты анализа
def lookupCustomer5(event):
    global varss, fotos
    if combovar.current() == 0:
        lblfoto.config(text=lang[91])  # указать путь фото с лицами
        lblbase.grid(row=2, column=0, sticky=W + NS, padx=(5, 5), pady=(5, 5))
        txtbase.grid(row=2, column=1, sticky=W + E, padx=(5, 5), pady=(5, 5))
        btnbase.grid(row=2, column=2, sticky=W, padx=(5, 5), pady=(5, 5))
        varss = '0'
        try:
            txtfoto.delete(0, END)
            fi = open(dir + '/pathlogfile.dat', 'r')
            ips = fi.readlines()
            fi.close()
            if ips != '':
                ips[0] = str(''.join(ips[0].split('\n')[:1]))
                fotopath = str('/'.join(ips[0].split("/")[:-1]))
                pths = dir + '/' + fotopath + '/crops/face/'
                txtfoto.insert(0, pths)
                fotos = txtfoto.get()
        except:
            txtfoto.delete(0, END)
    elif combovar.current() == 1:
        lblbase.grid_forget()
        txtbase.grid_forget()
        btnbase.grid_forget()
        lblfoto.config(text=lang[103])  # указать путь фото с номером авто
        varss = '1'
        try:
            txtfoto.delete(0, END)
            fi = open(dir + '/pathlogfile.dat', 'r')
            ips = fi.readlines()
            fi.close()
            if ips != '':
                ips[0] = str(''.join(ips[0].split('\n')[:1]))
                fotopath = str('/'.join(ips[0].split("/")[:-1]))
                pths = dir + '/' + fotopath + '/crops/licence/'
                txtfoto.insert(0, pths)
                fotos = txtfoto.get()
        except:
            txtfoto.delete(0, END)

# выбор выреза детектов
def check_cut():
    if combomodel.current() == 5 and cut_state.get() == True:
        chk3.grid(row=7, column=1, sticky=W, padx=(5, 5), pady=(5, 5))
    elif combomodel.current() == 6 and cut_state.get() == True:
        chk3.grid(row=7, column=1, sticky=W, padx=(5, 5), pady=(5, 5))
    else:
        chk3.grid_forget()

# выбор анализа выреза детектов
def check_anal():
    global varss
    if anal_state.get() == True and combomodel.current() == 5:
        varss = '0'
        lblbaseA.grid(row=8, column=0, sticky=W + NS, padx=(5, 5), pady=(5, 5))
        txtbaseA.grid(row=8, column=1, sticky=W + E, padx=(5, 5), pady=(5, 5))
        btnbaseA.grid(row=9, column=1, sticky=W, padx=(5, 5), pady=(5, 5))
    elif anal_state.get() == True and combomodel.current() == 6:
        varss = '1'
        lblbaseA.grid_forget()
        txtbaseA.grid_forget()
        btnbaseA.grid_forget()
    else:
        varss = '999'
        lblbaseA.grid_forget()
        txtbaseA.grid_forget()
        btnbaseA.grid_forget()

# Меню о программе
def about():
    messagebox.showinfo('About AIGod', 'AIGod v2.3\nbuild on February 09, 2023\n'
                                       'Open code: https://codeberg.org/krolaper/aigod\n\n'
                                       'Library: Yolov5 v7.0\n\nLicense: GNU, GPL-3.0 license\n\n'
                                       'Copyright © 2022-2023 JetBrains s.r.o.')

# Очистить историю результатов
def clear_history():
    try:
        path = dir+'/yolov5/runs'
        shutil.rmtree(path)
    except:
        messagebox.showinfo(lang[38], lang[41])
    try:
        histor = open(dir+'/history.dat', 'w')
        histor.write('')
        histor.close()
    except:
        messagebox.showinfo(lang[38], lang[40])

# Открыть историю результатов
def open_history():
    try:
        path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'yolov5/runs/detect')
        if platform == "linux" or platform == "linux2":
            subprocess.Popen(["xdg-open", path])
        elif platform == "darwin":
            subprocess.Popen(["open", path])
        elif platform == "win32":
            path = os.path.realpath(path)
            os.startfile(path)
    except:
        messagebox.showinfo(lang[38], lang[41])

# Обновить программу
def updates():
    opt = open(dir + '/option.dat', 'w')
    opt.write(str_lang_next + '\n' + theme_stand + '\n' + openRes)
    opt.close()
    
    if platform == "linux" or platform == "linux2":
        subprocess.run(dir+"/update")
    elif platform == "darwin":
        pass
    elif platform == "win32":
        subprocess.run(dir+"/update.exe")

# Действие при закрытии окна
def closes():
    opt = open(dir+'/option.dat', 'w')
    opt.write(str_lang_next+'\n'+theme_stand+'\n'+openRes)
    opt.close()

    dirs = dir+'/findface/esrgan/results/'
    try:
        for files in os.listdir(dirs):
            path = os.path.join(dirs, files)
            try:
                shutil.rmtree(path)
            except OSError:
                os.remove(path)
    except:
        pass

    delNameFile = ['/find.dat', '/resfind.dat', '/analizy.dat', '/objectsText.dat', '/objects.dat']
    for dell in delNameFile:
        try:
            os.remove(dir + dell)
        except OSError:
            pass
    window.destroy()

# Действие на выбор вкладки
def select_tab(event):
    tab_id = tab_control.select()
    tab_name = tab_control.tab(tab_id, "text")
    if tab_id == str('.!notebook.!frame'):
        try:
            txtfoto.delete(0, END)
            fi = open(dir + '/pathlogfile.dat', 'r')
            ips = fi.readlines()
            fi.close()
            if ips != '':
                ips[0] = str(''.join(ips[0].split('\n')[:1]))
                fotopath = str('/'.join(ips[0].split("/")[:-1]))
                pths = dir + '/' + fotopath + '/crops/face/'
                txtfoto.insert(0, pths)
                global fotos
                fotos = txtfoto.get()
        except:
            txtfoto.delete(0, END)

# Смена текста по языку
def change_language(lang):
    tab_control.tab(tab1, text=lang[92])
    tab_control.tab(tab2, text=lang[93])

    chk1.config(text=lang[77])
    chk2.config(text=lang[88])
    chk3.config(text=lang[93])
    chk4.config(text='9.' + lang[106])

    combo.config(state='normal')
    combomodel.config(state='normal')
    combocat.config(state='normal')
    comboobj.config(state='normal')
    combovar.config(state='normal')

    combo['values'] = (lang[0], lang[1], lang[2], lang[3])
    combo.current(0)

    combomodel['values'] = (lang[4], lang[8], lang[10], lang[68],
                            lang[71], lang[75], lang[99], lang[83]) #1,2,3,5 - lang[5], lang[6], lang[7], lang[9],
    combomodel.current(0)

    combocat['values'] = (lang[11], lang[12], lang[13], lang[14], lang[15], lang[16], lang[17], lang[18],
                          lang[19], lang[20], lang[21], lang[22], lang[23], lang[24], lang[25], lang[26])
    combocat.current(0)

    comboobj['values'] = (lang[27], lang[28])
    comboobj.current(0)

    combovar['values'] = (lang[101], lang[102])
    combovar.current(0)

    lbl.config(text=lang[29])
    lblobj1.config(text=lang[30])
    lblobj2.config(text=lang[31])
    btn.config(text=lang[32])
    btns.config(text=lang[33])
    lblformat.config(text=lang[80])
    lblmodel.config(text=lang[81])
    lblsize.config(text=lang[82])
    chk1.config(text=lang[77])
    chk2.config(text=lang[88])
    lblres.config(text=lang[97])
    btn2.config(text=lang[32])
    lblbase.config(text=lang[89])
    btnbase.config(text=lang[32])
    lblfoto.config(text=lang[91])
    btnfoto.config(text=lang[32])
    btns2.config(text=lang[33])
    lblresf.config(text=lang[97])
    lblvar.config(text=lang[100])
    chk3.config(text=lang[93])
    lblbaseA.config(text=lang[105])

    file_menu.entryconfig(0, label=lang[79])
    file_menu.entryconfig(1, label=lang[70])
    file_menu.entryconfig(2, label=lang[107])
    edit_menu.entryconfig(0, label=lang[84])
    edit_menu.entryconfig(1, label=lang[34])
    edit_menu.entryconfig(2, label=lang[108])
    themes_menu.entryconfig(0, label=lang[85])
    light_menu.entryconfig(0, label=lang[86])
    dark_menu.entryconfig(0, label=lang[87])
    program_menu.entryconfig(0, label=lang[35])
    program_menu.entryconfig(1, label=lang[78])
    for il in range(13):
        light_menu.entryconfig(il, label=themes_light[il])
    for id in range(5):
        dark_menu.entryconfig(id, label=themes_light[id])
    view_res.entryconfig(0, label=lang[109])
    view_res.entryconfig(1, label=lang[110])

    global str_lang_last, str_lang_next
    if str_lang_last == "en":
        menu.entryconfig("File", label=lang[36])
        menu.entryconfig("Edit", label=lang[84])
        menu.entryconfig("Help", label=lang[37])
        str_lang_last = str_lang_next
    elif str_lang_last == "ru":
        menu.entryconfig("Файл", label=lang[36])
        menu.entryconfig("Изменить", label=lang[84])
        menu.entryconfig("Справка", label=lang[37])
        str_lang_last = str_lang_next
    elif str_lang_last == "ua":
        menu.entryconfig("Файл", label=lang[36])
        menu.entryconfig("Змінити", label=lang[84])
        menu.entryconfig("Довідка", label=lang[37])
        str_lang_last = str_lang_next
    elif str_lang_last == "sp":
        menu.entryconfig("Archivo", label=lang[36])
        menu.entryconfig("Editar", label=lang[84])
        menu.entryconfig("Certificado", label=lang[37])
        str_lang_last = str_lang_next
    elif str_lang_last == "it":
        menu.entryconfig("File", label=lang[36])
        menu.entryconfig("Modificare", label=lang[84])
        menu.entryconfig("Certificate", label=lang[37])
        str_lang_last = str_lang_next
    elif str_lang_last == "gr":
        menu.entryconfig("Datei", label=lang[36])
        menu.entryconfig("Bearbeiten", label=lang[84])
        menu.entryconfig("Zertifikat", label=lang[37])
        str_lang_last = str_lang_next
    elif str_lang_last == "nr":
        menu.entryconfig("Fil", label=lang[36])
        menu.entryconfig("Redigere", label=lang[84])
        menu.entryconfig("Sertifikat", label=lang[37])
        str_lang_last = str_lang_next
    elif str_lang_last == "fr":
        menu.entryconfig("Fichier", label=lang[36])
        menu.entryconfig("Éditer", label=lang[84])
        menu.entryconfig("Certificat", label=lang[37])
        str_lang_last = str_lang_next

    combo.config(state='readonly')
    combomodel.config(state='readonly')
    combocat.config(state='readonly')
    comboobj.config(state='readonly')
    combovar.config(state='readonly')

    txtscroll.config(state='normal')
    lookupCustomer('')
    lookupCustomer2('')
    lookupCustomer3('')
    lookupCustomer1('')
    lookupCustomer5('')
    txtscroll.config(state='disabled')

    opt = open(dir + '/option.dat', 'w')
    opt.write(str_lang_next + '\n' + theme_stand + '\n' + openRes)
    opt.close()

# Выбор языка
def lang_s(coun):
    global lang, str_lang_next
    str_lang_next = coun
    if coun == "en":
        lang = english
        change_language(english)
    elif coun == "ru":
        lang = russian
        change_language(russian)
    elif coun == "ua":
        lang = ukraine
        change_language(ukraine)
    elif coun == "sp":
        lang = spanish
        change_language(spanish)
    elif coun == "it":
        lang = italian
        change_language(italian)
    elif coun == "gr":
        lang = german
        change_language(german)
    elif coun == "nr":
        lang = norwegian
        change_language(norwegian)
    elif coun == "fr":
        lang = french
        change_language(french)

# Смена темы окон
def change_theme(them):
    global theme_stand, style
    style.theme_use(them)
    theme_stand = them
    window.update()

    opt = open(dir + '/option.dat', 'w')
    opt.write(str_lang_next + '\n' + theme_stand + '\n' + openRes)
    opt.close()

# Выбор темы
def tml(nm):
    change_theme(themes_light[nm])
def tmd(nm):
    change_theme(themes_dark[nm])

# доп меню на правую клавишу
def popup(event):
    global x, y
    x = event.x
    y = event.y
    mn.tk_popup(event.x_root, event.y_root) #tk_popup - зажимать #post - нажать

# фокус позиции доп меню
def focusPos(texts):
    global entrys
    entrys = texts

# доп меню Копировать
def copy_fun():
    if entrys == 'txt':
        #window.withdraw()
        window.clipboard_clear()
        window.clipboard_append(txt.get())
    elif entrys == 'txtobj':
        window.clipboard_clear()
        window.clipboard_append(txtobj.get())
    elif entrys == 'txt2':
        window.clipboard_clear()
        window.clipboard_append(txt2.get())
    elif entrys == 'txtres':
        window.clipboard_clear()
        window.clipboard_append(txtres.get())
    elif entrys == 'txtfoto':
        window.clipboard_clear()
        window.clipboard_append(txtfoto.get())
    elif entrys == 'txtbase':
        window.clipboard_clear()
        window.clipboard_append(txtbase.get())
    elif entrys == 'wikiscroll':
        window.clipboard_clear()
        window.clipboard_append(wikiscroll.get())
    else:
        pass

# доп меню Вставить
def paste_fun():
    if entrys == 'txt':
        #window.withdraw()
        txt.delete(0, END)
        txt.insert(0, window.clipboard_get())
    elif entrys == 'txtobj':
        txtobj.delete(0, END)
        txtobj.insert(0, window.clipboard_get())
    elif entrys == 'txt2':
        txt2.delete(0, END)
        txt2.insert(0, window.clipboard_get())
    elif entrys == 'txtres':
        txtres.delete(0, END)
        txtres.insert(0, window.clipboard_get())
    elif entrys == 'txtfoto':
        txtfoto.delete(0, END)
        txtfoto.insert(0, window.clipboard_get())
    elif entrys == 'txtbase':
        txtbase.delete(0, END)
        txtbase.insert(0, window.clipboard_get())
    else:
        pass

def viewRes(vProg):
    global openRes
    openRes = vProg

    opt = open(dir + '/option.dat', 'w')
    opt.write(str_lang_next + '\n' + theme_stand + '\n' + openRes)
    opt.close()

# Получить список разрешений экрана
list1 = []
pygame.init()
running = True
while running:
    for mode in pygame.display.list_modes():
        w, h = mode[0], mode[1]
        list1.append("{}.{}".format(w, h))
    running = False
pygame.quit()

lang = None
str_lang_last = "en"
str_lang_next = "en"
theme_stand = 'cosmo'
style = None
openRes='default'
th = 0
ln = 0
opr = 0

# языки
english = []
russian = []
ukraine = []
spanish = []
italian = []
german = []
norwegian = []
french = []

# загрузка языков
name_langs = ['english', 'russian', 'ukrainian', 'spanish', 'italian', 'german', 'norwegian', 'french',] #0-7
for o in range(8):
    spis = open(dir + '/bin/lang/'+name_langs[o]+'.dat', 'r', encoding='utf8')
    lng = spis.readlines()
    spis.close()
    for la in lng:
        la = str('\n'.join(la.split("\n")[:-1]))
        if la.find('\\n') > -1:
            la = la.replace('\\n', '\n')
        if o == 0:
            english.append(la)
        elif o == 1:
            russian.append(la)
        elif o == 2:
            ukraine.append(la)
        elif o == 3:
            spanish.append(la)
        elif o == 4:
            italian.append(la)
        elif o == 5:
            german.append(la)
        elif o == 6:
            norwegian.append(la)
        elif o == 7:
            french.append(la)

#определить тему, язык программы, отображение результата
try:
    rf = open(dir+'/option.dat', 'r')
    oplist = rf.readlines()
    rf.close()

    # проверка настройки языка
    str_lang_next = str(''.join(oplist[0].split('\n', 1)))
    str_lang_last = str_lang_next
    if str_lang_next == 'en':
        lang = english
        ln = 0
    elif str_lang_next == 'ru':
        lang = russian
        ln = 1
    elif str_lang_next == 'ua':
        lang = ukraine
        ln = 2
    elif str_lang_next == 'sp':
        lang = spanish
        ln = 3
    elif str_lang_next == 'it':
        lang = italian
        ln = 4
    elif str_lang_next == 'gr':
        lang = german
        ln = 5
    elif str_lang_next == 'nr':
        lang = norwegian
        ln = 6
    elif str_lang_next == 'fr':
        lang = french
        ln = 7

    # проверка темы
    theme_stand = str(''.join(oplist[1].split('\n', 1)))

    if theme_stand == 'cosmo':
        th = 0
    elif  theme_stand == 'flatly':
        th = 1
    elif  theme_stand == 'journal':
        th = 2
    elif  theme_stand == 'litera':
        th = 3
    elif  theme_stand == 'lumen':
        th = 4
    elif  theme_stand == 'minty':
        th = 5
    elif  theme_stand == 'pulse':
        th = 6
    elif  theme_stand == 'sandstone':
        th = 7
    elif  theme_stand == 'united':
        th = 8
    elif  theme_stand == 'yeti':
        th = 9
    elif  theme_stand == 'morph':
        th = 10
    elif  theme_stand == 'simplex':
        th = 11
    elif  theme_stand == 'cerculean':
        th = 12
    elif  theme_stand == 'solar':
        th = 13
    elif  theme_stand == 'superhero':
        th = 14
    elif  theme_stand == 'darkly':
        th = 15
    elif  theme_stand == 'cyborg':
        th = 16
    elif  theme_stand == 'vapor':
        th = 17

    # проверка  выбора отображения результата
    openRes = str(''.join(oplist[2].split('\n', 1)))
    if openRes == 'default':
        opr = 0
    elif openRes == 'unic':
        opr = 1
except IOError:
    str_lang_last = "en"
    str_lang_next = "en"
    lang = english
    ln = 0

    theme_stand = 'cosmo'
    th = 0

    openRes = 'default'
    opr = 0

window = ttk.Window(title="AIGod", themename=theme_stand, iconphoto=dir+"/bin/image/logo.png", size=[880, 570],
                    resizable=[0, 0])
themes_light = ['cosmo', 'flatly', 'journal', 'litera', 'lumen', 'minty', 'pulse', 'sandstone', 'united', 'yeti', 'morph',
                'simplex', 'cerculean']
themes_dark = ['solar', 'superhero', 'darkly', 'cyborg', 'vapor']

style = ttk.Style()
style.theme_use(theme_stand)

# проверка существования файла
if os.path.exists(dir+'/history.dat') == True:
    pass
else:
    wrs = open(dir + '/history.dat', 'w')
    wrs.write("")
    wrs.close()

# Создать вкладки
tab_control = Notebook(window)
tab1 = Frame(tab_control)
tab2 = Frame(tab_control)
tab_control.add(tab1, text=lang[92])
tab_control.add(tab2, text=lang[93])

# Получить список разрешений экрана
lists = list(set(list1))
lists.sort(key=float, reverse=True)
combosize = Combobox(tab1, state='normal')
combosize['values'] = (lists)
combosize.current(0)
combosize.config(state='readonly')
combosize.grid(row=5, column=1, sticky=W, padx=(5,5), pady=(5,5))

namefile = ''
selected = IntVar()
detect_state = BooleanVar()
cut_state = BooleanVar()
anal_state = BooleanVar()
wiki_state = BooleanVar()
liner = ''
sz = combosize.get()
mw = ''.join(sz.split('.')[0]) #800
mh = ''.join(sz.split('.')[1]) #600

model=dir+"/yolov5/modres/Objects365.pt"
classs=""
url=""
newCheck=""
bases=''
fotos=""
varss='0'

# Выбор формата работы
combo = Combobox(tab1, state='normal')
combo['values'] = (lang[0], lang[1], lang[2], lang[3])
combo.current(0)
combo.config(state='readonly')
combo.grid(row=0, column=1, sticky=W, padx=(5,5), pady=(5,5))

# Выбор формата обработки
comboobj = Combobox(tab1, state='normal')
comboobj['values'] = (lang[27], lang[28])
comboobj.current(0)
comboobj.config(state='readonly')
selected.set(1)
comboobj.grid(row=0, column=2, sticky=W, padx=(5,5), pady=(5,5))

# Выбор модели
combomodel = Combobox(tab1, state='normal')
combomodel['values'] = (lang[4], lang[8], lang[10], lang[68], lang[71],
                        lang[75], lang[99], lang[83])
combomodel.current(0)
combomodel.config(state='readonly')
combomodel.grid(row=2, column=1, sticky=W, padx=(5,5), pady=(5,5))

# Выбор категории
combocat = Combobox(tab1, state='normal')
combocat['values'] = (lang[11], lang[12], lang[13], lang[14], lang[15], lang[16], lang[17], lang[18], lang[19],
                      lang[20], lang[21], lang[22], lang[23], lang[24], lang[25], lang[26])
combocat.current(0)
combocat.config(state='readonly')
combocat.grid(row=2, column=2, sticky=W, padx=(5,5), pady=(5,5))

detect_state.set(False)  # проверка состояния чекбокса детекта
chk1 = Checkbutton(tab1, text=lang[77], var=detect_state) # уведомление

cut_state.set(False)  # проверка состояния чекбокса выреза
chk2 = Checkbutton(tab1, text=lang[88], var=cut_state, command=check_cut) # вырезать детект
chk2.grid(row=7, column=0, sticky=W, padx=(5, 5), pady=(5, 5))

anal_state.set(False)  # проверка состояния чекбокса анализа
chk3 = Checkbutton(tab1, text=lang[93], var=anal_state, command=check_anal) # анализ вырезаного детекта

wiki_state.set(False)  # проверка состояния чекбокса wiki
chk4 = Checkbutton(tab1, text='9.'+lang[106], var=wiki_state) # wiki объектов # command=check_wiki
chk4.grid(row=10, column=0, sticky=W, padx=(5,5), pady=(5,5))

# Выбор варианта анализа
combovar = Combobox(tab2, state='normal')
combovar['values'] = (lang[101], lang[102])
combovar.current(0)
combovar.config(state='readonly')
combovar.grid(row=0, column=1, sticky=W, padx=(5,5), pady=(5,5))

# лейблы, инпуты, кнопки
lbl = Label(tab1, text=lang[29]) # указать путь к файлу
txtscroll = scrolledtext.ScrolledText(tab1, width=20, height=20) # список объектов
txtscroll.insert(INSERT, lang[51])
txtscroll.config(state='disabled')
lblobj1 = Label(tab1, text=lang[30]) # указать номер объекта
lblobj2 = Label(tab1, text=lang[31]) # указать номер объекта
txt = Entry(tab1, width=20) # путь к файлу
txtobj = Entry(tab1, width=20) # путь к объекту
btn = Button(tab1, text=lang[32], command=pathing1) # выбор файла
btns = Button(tab1, text=lang[33], command=startdetect) # старт
lblres = Label(tab1, text=lang[97]) # путь к результату
txtres = Entry(tab1, width=20) # путь к результату
lblformat = Label(tab1, text=lang[80]) # формат файла
lblmodel = Label(tab1, text=lang[81]) # модель объектов
lblsize = Label(tab1, text=lang[82]) # размер окна результата
lblresf = Label(tab1, text=lang[97]) # путь к результату
lblbaseA = Label(tab1, text=lang[105]) # указать путь к базе
txtbaseA = Entry(tab1, width=20) # путь к базе
btnbaseA = Button(tab1, text=lang[32], command=pathing6) # выбор папки базы
lblformat.grid(row=0, column=0, sticky=W+NS, padx=(5,5), pady=(5,5))
lbl.grid(row=1, column=0, sticky=W+NS, padx=(5,5), pady=(5,5))
txt.grid(row=1, column=1, sticky=W+E, padx=(5,5), pady=(5,5))
btn.grid(row=1, column=2, sticky=W+NS, padx=(5,5), pady=(5,5))
lblmodel.grid(row=2, column=0, sticky=W+NS, padx=(5,5), pady=(5,5))
lblobj1.grid(row=4, column=0, sticky=W+NS, padx=(5,5), pady=(5,5))
txtobj.grid(row=4, column=1, sticky=W+E, padx=(5,5), pady=(5,5))
txtscroll.grid(row=4, column=2, rowspan=21, sticky=W+NS+E, padx=(5,5), pady=(5,5)) # слитие ячеек 3+18(пустых)
lblsize.grid(row=5, column=0, sticky=W+NS, padx=(5,5), pady=(5,5))
txt2 = Entry(tab1, width=20) # путь к моделе
btn2 = Button(tab1, text=lang[32], command=pathing3) # выбор файла модели


lblbase = Label(tab2, text=lang[89]) # указать путь к базе
txtbase = Entry(tab2, width=20) # путь к базе
btnbase = Button(tab2, text=lang[32], command=pathing4) # выбор папки базы
lblfoto = Label(tab2, text=lang[91]) # указать путь фото с лицами
txtfoto = Entry(tab2, width=20) # путь к фото с лицами
btnfoto = Button(tab2, text=lang[32], command=pathing5) # выбор папки фото с лицами
btns2 = Button(tab2, text=lang[33], command=start2) # старт2
lblvar = Label(tab2, text=lang[100]) # выбор анализа фото
lblvar.grid(row=0, column=0, sticky=W+NS, padx=(5, 5), pady=(5, 5))
lblfoto.grid(row=1, column=0, sticky=W+NS, padx=(5, 5), pady=(5, 5))
txtfoto.grid(row=1, column=1, sticky=W + E, padx=(5, 5), pady=(5, 5))
btnfoto.grid(row=1, column=2, sticky=W, padx=(5, 5), pady=(5, 5))
lblbase.grid(row=2, column=0, sticky=W+NS, padx=(5, 5), pady=(5, 5))
txtbase.grid(row=2, column=1, sticky=W + E, padx=(5, 5), pady=(5, 5))
btnbase.grid(row=2, column=2, sticky=W, padx=(5, 5), pady=(5, 5))
btns2.grid(row=25, column=2, sticky=E, padx=(5, 5), pady=(5, 5))

wikiscroll = scrolledtext.ScrolledText(tab2, width=105, height=20) # список объектов
wikiscroll.grid(row=26, column=0, columnspan=3, sticky=W+NS+E, padx=(5,5), pady=(5,5))


# Реакция на выбор в комбобоксах
combo.bind("<<ComboboxSelected>>", lookupCustomer)
combomodel.bind("<<ComboboxSelected>>", lookupCustomer2)
combocat.bind("<<ComboboxSelected>>", lookupCustomer3)
comboobj.bind("<<ComboboxSelected>>", lookupCustomer1)
combosize.bind("<<ComboboxSelected>>", lookupCustomer4)
combovar.bind("<<ComboboxSelected>>", lookupCustomer5)

# Реакция на выбор вкладки
tab_control.pack(expand=1, fill='both')
tab_control.enable_traversal()
tab_control.bind("<Button-1>", select_tab)

mn = Menu(tearoff=0)
mn.add_command(label=lang[95], command=copy_fun)
mn.add_command(label=lang[96], command=paste_fun)
x = 0
y = 0
entrys = ''
txt.bind('<Button-3>', popup)
txt.bind('<Button-3>', lambda event: focusPos('txt'), add='+')
txtobj.bind('<Button-3>', popup)
txtobj.bind('<Button-3>', lambda event: focusPos('txtobj'), add='+')
txt2.bind('<Button-3>', popup)
txt2.bind('<Button-3>', lambda event: focusPos('txt2'), add='+')
txtres.bind('<Button-3>', popup)
txtres.bind('<Button-3>', lambda event: focusPos('txtres'), add='+')
txtfoto.bind('<Button-3>', popup)
txtfoto.bind('<Button-3>', lambda event: focusPos('txtfoto'), add='+')
txtbase.bind('<Button-3>', popup)
txtbase.bind('<Button-3>', lambda event: focusPos('txtbase'), add='+')
wikiscroll.bind('<Button-3>', popup)
wikiscroll.bind('<Button-3>', lambda event: focusPos('wikiscroll'), add='+')

var_theme = IntVar(value=th)
var_lang = IntVar(value=ln)
var_viewRes = IntVar(value=opr)
# Меню
menu = Menu(window)
file_menu = Menu(menu, tearoff=0)
file_menu.add_command(label=lang[79], command=open_history)
file_menu.add_command(label=lang[70], command=clear_history)
file_menu.add_command(label=lang[107], command=closes)
view_res = Menu(menu, tearoff=0)
view_res.add_radiobutton(label=lang[109], command=lambda: viewRes('default'), variable=var_viewRes, value=0)
view_res.add_radiobutton(label=lang[110], command=lambda: viewRes('unic'), variable=var_viewRes, value=1)
lang_menu = Menu(menu, tearoff=0)
lang_menu.add_radiobutton(label='English', command=lambda: lang_s('en'), variable=var_lang, value=0)
lang_menu.add_radiobutton(label='Русский', command=lambda: lang_s('ru'), variable=var_lang, value=1)
lang_menu.add_radiobutton(label='Українська', command=lambda: lang_s('ua'), variable=var_lang, value=2)
lang_menu.add_radiobutton(label='Español', command=lambda: lang_s('sp'), variable=var_lang, value=3)
lang_menu.add_radiobutton(label='Italiano', command=lambda: lang_s('it'), variable=var_lang, value=4)
lang_menu.add_radiobutton(label='Deutsch', command=lambda: lang_s('gr'), variable=var_lang, value=5)
lang_menu.add_radiobutton(label='Norsk', command=lambda: lang_s('nr'), variable=var_lang, value=6)
lang_menu.add_radiobutton(label='Français', command=lambda: lang_s('fr'), variable=var_lang, value=7)
light_menu = Menu(menu, tearoff=0)
light_menu.add_radiobutton(label=themes_light[0], command=lambda: tml(0), variable=var_theme, value=0)
light_menu.add_radiobutton(label=themes_light[1], command=lambda: tml(1), variable=var_theme, value=1)
light_menu.add_radiobutton(label=themes_light[2], command=lambda: tml(2), variable=var_theme, value=2)
light_menu.add_radiobutton(label=themes_light[3], command=lambda: tml(3), variable=var_theme, value=3)
light_menu.add_radiobutton(label=themes_light[4], command=lambda: tml(4), variable=var_theme, value=4)
light_menu.add_radiobutton(label=themes_light[5], command=lambda: tml(5), variable=var_theme, value=5)
light_menu.add_radiobutton(label=themes_light[6], command=lambda: tml(6), variable=var_theme, value=6)
light_menu.add_radiobutton(label=themes_light[7], command=lambda: tml(7), variable=var_theme, value=7)
light_menu.add_radiobutton(label=themes_light[8], command=lambda: tml(8), variable=var_theme, value=8)
light_menu.add_radiobutton(label=themes_light[9], command=lambda: tml(9), variable=var_theme, value=9)
light_menu.add_radiobutton(label=themes_light[10], command=lambda: tml(10), variable=var_theme, value=10)
light_menu.add_radiobutton(label=themes_light[11], command=lambda: tml(11), variable=var_theme, value=11)
light_menu.add_radiobutton(label=themes_light[12], command=lambda: tml(12), variable=var_theme, value=12)
dark_menu = Menu(menu, tearoff=0)
dark_menu.add_radiobutton(label=themes_dark[0], command=lambda: tmd(0), variable=var_theme, value=13) #*.add_checkbutton(label='', command=*, onvalue=1, offvalue=0)
dark_menu.add_radiobutton(label=themes_dark[1], command=lambda: tmd(1), variable=var_theme, value=14)
dark_menu.add_radiobutton(label=themes_dark[2], command=lambda: tmd(2), variable=var_theme, value=15)
dark_menu.add_radiobutton(label=themes_dark[3], command=lambda: tmd(3), variable=var_theme, value=16)
dark_menu.add_radiobutton(label=themes_dark[4], command=lambda: tmd(4), variable=var_theme, value=17)
themes_menu = Menu(menu, tearoff=0)
themes_menu.add_cascade(label=lang[86], menu=light_menu)
themes_menu.add_cascade(label=lang[87], menu=dark_menu)
edit_menu = Menu(menu, tearoff=0)
edit_menu.add_cascade(label=lang[85], menu=themes_menu)
edit_menu.add_cascade(label=lang[34], menu=lang_menu)
edit_menu.add_cascade(label=lang[108], menu=view_res)
program_menu = Menu(menu, tearoff=0)
program_menu.add_command(label=lang[35], command=about)
program_menu.add_command(label=lang[78], command=updates)
menu.add_cascade(label=lang[36], menu=file_menu)
menu.add_cascade(label=lang[84], menu=edit_menu)
menu.add_cascade(label=lang[37], menu=program_menu)
window.config(menu=menu)

# Закрытие окна
window.protocol('WM_DELETE_WINDOW', closes)

# Отрисовка окна
window.mainloop()