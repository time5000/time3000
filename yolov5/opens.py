import os
from sys import platform
if platform == "win32":
    from winreg import *
import subprocess
from tkinter import *
import ttkbootstrap as ttk
from ttkbootstrap.constants import *

dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))
pth = dir.replace('yolov5', '')

prog = []
fls = ''
window = None

def start(num):
    #image_unpack = prog[num] #программа автозапуска: Windows - 'start', Linux — 'see' #'/usr/share/applications/'
    if platform == "linux" or platform == "linux2":
        image_unpack = str(''.join(prog[num].split('.desktop')[:1]))
    elif platform == "win32":
        image_unpack = prog[num]
        disk = os.getenv("SystemDrive") # опреелить системный диск
        if image_unpack.find('%ProgramFiles(x86)%') >= 0:
            image_unpack = image_unpack.replace('%ProgramFiles(x86)%', disk+'\\Program Files (x86)')
        if image_unpack.find('%ProgramFiles)%') >= 0:
            image_unpack = image_unpack.replace('%ProgramFiles%', disk+'\\Program Files')

        global fls
        if fls.find('/') >= 0:
            fls = fls.replace('/', '\\')
    file_path = fls
    subprocess.check_call([image_unpack, file_path])
    look(cl=True)

def look(cl):
    global window
    if cl == True:
        window.destroy()
    else:
        global prog
        prog = []
        if platform == "linux" or platform == "linux2":
            lin()
        elif platform == "darwin":
            pass
        elif platform == "win32":
            win()

        class AutoScrollbar(Scrollbar):
            def set(self, low, high):
                if float(low) <= 0.0 and float(low) >= 1.0:
                    self.grid_remove()
                else:
                    self.grid()
                    Scrollbar.set(self, low, low)

            def pack(self, **kw):
                raise TclError('Cannot use pack with the widget ')

            def place(self, **kw):
                raise TclError('Cannot use place with the widget ')

        try:
            rf = open(pth + 'option.dat', 'r')
            oplist = rf.readlines()
            rf.close()

            theme_stand = str(''.join(oplist[1].split('\n', 1)))
        except IOError:
            theme_stand = 'cosmo'

        window = ttk.Toplevel(title="AIGod", iconphoto=pth + "bin/image/logo.png", size=[150, 200],
                               resizable=[0, 0])
        ttk.Style().theme_use(theme_stand)

        hbar = AutoScrollbar(window, orient='horizontal')
        vbar = AutoScrollbar(window, orient='vertical')
        vbar.grid(row=0, column=1, sticky='ns')
        hbar.grid(row=1, column=0, sticky='we')
        canvas = Canvas(window, highlightthickness=0, xscrollcommand=hbar.set, yscrollcommand=vbar.set)
        canvas.grid(row=0, column=0, sticky='nswe')
        canvas.update()
        hbar.configure(command=canvas.xview)
        vbar.configure(command=canvas.yview)
        window.grid_rowconfigure(0, weight=1)
        window.grid_columnconfigure(0, weight=1)
        frame = Frame(canvas)
        frame.rowconfigure(1, weight=1)
        frame.columnconfigure(1, weight=1)

        # имена переменных для новых виджетов
        nameswid = []
        for i in range(len(prog)):
            nameswid.append('nameWid' + str(i))

        # текст виджета (имена програм)
        tx = []
        for txt in prog:
            try:
                if platform == "linux" or platform == "linux2":
                    txt = txt.split('.')[:-1]
                    txt = txt[-1:]
                elif platform == "win32":
                    while True:
                        try:
                            txt = txt.split('\\', 1)[1]
                        except:
                            break
                    txt = txt.split('.')[:-1]
                tx.append(txt)
            except:
                pass

        # добавление довых виджетов
        for j in range(len(prog)):
            nameswid[j] = Button(frame, text=tx[j], command=lambda j=j: start(j)).grid(row=j, column=0, sticky=W+E)
            window.update()

        canvas.create_window(0, 0, anchor=NW, window=frame)
        frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

        window.mainloop()

def checks():
    f = open(pth + 'pathlogfile.dat', 'r')
    ip = f.readlines()
    f.close()

    typ = str(''.join(ip[0].split('\n')[:1]))

    global fls
    fls = pth + typ

    if typ != '':
        typ = str(''.join(typ.split('.', 1)[1]))

    return typ

def lin():
    tp = checks()
    p1 = '/usr/share/applications/mimeinfo.cache'

    opt = open(p1, 'r')
    rrr = opt.readlines()
    opt.close()

    for strok in rrr:
        if strok.find('image') > -1 or strok.find('video') > -1:
            if strok.find('/'+tp+'=') > -1:
                strok = str(''.join(strok.split('=', 1)[1]))
                strok = str(''.join(strok.split('\n')[:1]))
                for nm in strok.split(';'):
                    if nm != '':
                        prog.append(nm)

def win():
    tp = checks()
    aReg = ConnectRegistry(None,HKEY_CLASSES_ROOT)
    aKey = OpenKey(aReg, r"Applications")
    i=0
    listdir = []
    while True:
        try:
            keydir = EnumKey(aKey, i)
            listdir.append(keydir)
            #yield keydir
            i+=1
        except WindowsError as e:
            break
    for dirr in listdir:
        try:
            aKey2 = OpenKey(aKey, dirr+r"\SupportedTypes")
            s=0
            listtype = []
            while True:
                try:
                    keyname = EnumValue(aKey2, s)
                    listtype.append(keyname[0]) #0-Имя поля, 1-Значение, 2-Тип данных
                    #yield keydir
                    s+=1
                except WindowsError as e:
                    break
            if len(listtype) != 0:
                for typs in listtype:
                    if typs == '.'+tp:
                        aKey3 = OpenKey(aKey, dirr+r"\shell\open\command")
                        keyname2 = EnumValue(aKey3, 0)
                        dirprog = keyname2[1]
                        if dirprog != '':
                            dirprog = dirprog.replace('"', '')
                            dirprog = str(''.join(dirprog.split('.exe')[:1]))
                            prog.append(dirprog+'.exe')
        except:
            pass
