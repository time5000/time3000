# AIGod by AIGod, GPL-3.0 license
import os
import datetime
import tkinter as tk
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
import numpy as np
import cv2
from PIL import Image, ImageTk

dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))
pth = dir.replace('yolov5', '')

tots, i, fps = 0, 0, 0

def vidmain(mw, mh):
    f = open(pth + 'pathlogfile.dat', 'r')
    vp = f.readlines()
    f.close()

    if vp[len(vp)-1] == "/":
        vp = vp[:len(vp)-1]

    video_name = str(''.join(vp[0].split('\n')[:1]))

    def update_duration():
        """ updates the duration after finding the duration """
        duration = int(tots)-1
        end_time["text"] = str(datetime.timedelta(seconds=duration))
        progress_slider["to"] = duration
        window1.update()

    def update_scale():
        """ updates the scale value """
        progress_value.set(i)
        start_time.config(text=str(datetime.timedelta(seconds=i)))
        window1.update()

    def plays():
        global i, fps
        if i == tots - 1:
            i=0
        cap.set(cv2.CAP_PROP_POS_FRAMES, i)

        _, frame = cap.read()
        cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        sz = str(window1.geometry())
        sz = str(''.join(sz.split('+')[0]))
        ww = int(''.join(sz.split('x')[0]))
        hh = int(''.join(sz.split('x', 1)[1]))

        if ww < 4:
            ww = 4
        if hh < 150:
            hh = 150

        img = Image.fromarray(cv2image).resize((ww-2, hh-75))
        imgtk = ImageTk.PhotoImage(image=img)
        display.imgtk = imgtk
        display.configure(image=imgtk)
        update_duration()
        update_scale()
        if play_pause_btn["text"] == "Play":
            i += 1
        elif play_pause_btn["text"] == "Pause":
            i = i
        window1.update()
        window1.after(0, plays)

    def seek(value):
        """ used to seek a specific timeframe """
        global i
        i = int(value)
        progress_value.set(i)
        seek(int(value))
        window1.update()

    def skip(value: int):
        """ skip seconds """
        global i
        if value == -5:
            i -= 5
        elif value == 5:
            i += 5
        progress_value.set(progress_slider.get() + value)
        seek(int(progress_slider.get()) + value)
        window1.update()

    def play_pause():
        global i
        """ pauses and plays """
        if play_pause_btn["text"] == "Play":
            #i = progress_slider.get()
            play_pause_btn["text"] = "Pause"
        elif play_pause_btn["text"] == "Pause":
            play_pause_btn["text"] = "Play"
        window1.update()

    # def video_ended():
    #     """ handle video ended """
    #     progress_slider.set(progress_slider["to"])
    #     play_pause_btn["text"] = "Play"
    #     progress_slider.set(0)
    #     start_time.config(text=str(datetime.timedelta(seconds=0)))
    #     window1.update()

    def closes():
        cap.release()
        cv2.destroyAllWindows()
        window1.destroy()

    # проверка темы
    try:
        rf = open(pth + 'option.dat', 'r')
        oplist = rf.readlines()
        rf.close()

        theme_stand = str(''.join(oplist[1].split('\n', 1)))
    except IOError:
        theme_stand = 'cosmo'

    window1 = ttk.Toplevel(title="AIGod", iconphoto=pth + "bin/image/logo.png", size=[mw, mh])
    ttk.Style().theme_use(theme_stand)

    display = tk.Label(window1)
    display.pack(expand=True, fill="both")

    play_pause_btn = tk.Button(window1, text="Play", command=play_pause)
    play_pause_btn.pack()

    skip_plus_5sec = tk.Button(window1, text="Skip -5 sec", command=lambda: skip(-5))
    skip_plus_5sec.pack(side="left")

    start_time = tk.Label(window1, text=str(datetime.timedelta(seconds=0)))
    start_time.pack(side="left")

    progress_value = tk.IntVar(window1)

    progress_slider = tk.Scale(window1, variable=progress_value, from_=0, to=0, orient="horizontal", command=seek)
    progress_slider.pack(side="left", fill="x", expand=True)

    end_time = tk.Label(window1, text=str(datetime.timedelta(seconds=0)))
    end_time.pack(side="left")

    skip_plus_5sec = tk.Button(window1, text="Skip +5 sec", command=lambda: skip(5))
    skip_plus_5sec.pack(side="left")

    if video_name != '':
        global i, tots, fps
        cap = cv2.VideoCapture(pth+video_name)
        tots = cap.get(cv2.CAP_PROP_FRAME_COUNT)
        i = 0
        window1.geometry(str(mw)+'x'+str(mh))
        fps = cap.get(cv2.CAP_PROP_FPS)
        progress_slider.config(to=0, from_=0)
        play_pause_btn["text"] = "Play"
        progress_value.set(0)
        plays()

    window1.protocol('WM_DELETE_WINDOW', closes)

    window1.update()