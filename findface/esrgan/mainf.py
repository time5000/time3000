# AIGod by AIGod, GPL-3.0 license
import os 
import torch
import numpy as np
from findface.esrgan.RealESRGAN import RealESRGAN

from tkinter import *
from tkinter.ttk import Progressbar
import ttkbootstrap as ttk
from ttkbootstrap.constants import *

from PIL import Image
from sys import platform

from findface.findimg import faces
from findtext.readtexts import text_recognition

dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))
pth = dir.replace('findface/esrgan', '')


def save_check(j, sr_image):
    if os.path.isfile(dir + f'/results/{j}.png') == False:
        sr_image.save(dir + f'/results/{j}.png')
    else:
        j = j+1 # * 2 + 1
        save_check(j, sr_image)

def redag_imgf(img, path):
    path_old = path
    path = str(''.join(path.split('.')[0]))
    try:
        img = img.convert('RGB')
        img.save(path+'.jpeg')
        os.remove(path_old)
    except:
        pass

def redag_imgs(img, path):
    img = img.reduce(2)
    img.save(path)

def mainf(varr) -> int:
    global pth
    if platform == "win32":
        pth = dir.replace('findface\\esrgan', '')

    rr = open(pth + 'analizy.dat', 'r')
    phs = rr.readlines()
    rr.close()

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = RealESRGAN(device, scale=4)

    #phs[0] = str(''.join(phs[0].split('\n')[:1]))
    if varr == '0': # лица    # phs[0]
        weights = 'RealESRGAN_x4.pth'
    elif varr == '1': # текст
        weights = 'RealESRGAN_x4plus.pth' #'4x-AnimeSharp' #.pth

    model.load_weights(dir+'/weights/'+weights, download=True)

    if os.path.exists(dir + '/objects') == True:
        pass
    else:
        os.makedirs(dir + '/objects')

    if os.path.exists(dir + '/results') == True:
        pass
    else:
        os.makedirs(dir + '/results')

    phs[1] = str(''.join(phs[1].split('\n')[:1]))
    for j, imag in enumerate(os.listdir(phs[1])): # enumerate(os.listdir("inputs"))
        ptt = phs[1]+f'{imag}'
        imag = Image.open(phs[1]+f'{imag}')
        formats = imag.format
        sizes = imag.size
        if formats == 'jpeg' or formats == 'JPEG' or formats == 'jpg' or formats == 'JPG':
            pass
        else:
            redag_imgf(imag, ptt)
            mainf()
        if sizes[0] > 300 or sizes[1]>300:
            redag_imgs(imag, ptt)
            mainf()

    # проверка темы
    try:
        ff = open(pth + 'option.dat', 'r')
        oplist = ff.readlines()
        ff.close()

        theme_stand = str(''.join(oplist[1].split('\n', 1)))
    except IOError:
        theme_stand = 'cosmo'
    window1 = ttk.Toplevel(title="AIGod", iconphoto=pth + "bin/image/logo.png",
                           resizable=[0, 0])
    ttk.Style().theme_use(theme_stand)
    # лейблы, инпуты, кнопки
    lbl = Label(window1, text='Face Photo Processing: 0/0')
    bar = Progressbar(window1, maximum=0)
    bar['value'] = 0
    bar.grid(row=0, column=0, sticky=W + E, padx=(5, 5), pady=(5, 5))
    window1.update()

    maxx = len(os.listdir(phs[1]))
    lbl.config(text='Face Photo Processing: ' + str(0) + '/' + str(maxx))
    lbl.grid(row=1, column=0, padx=(5, 5), pady=(5, 5))
    window1.update()

    for i, image in enumerate(os.listdir(phs[1])): # enumerate(os.listdir("inputs"))
        flimg = image
        image = Image.open(phs[1]+f'{image}').convert('RGB') # f"inputs/{image}"
        try:
            sr_image = model.predict(image)
            save_check(i, sr_image)
        except:
            pass

        wrfile = open(pth + 'find.dat', 'a')
        res = phs[1]+f'{flimg}' #+'====='+ dir+f'/results/{i}.png'
        wrfile.write(res+'\n')
        wrfile.close()

        # прогрессбар
        cr = i+1
        lbl.config(text='Face Photo Processing: ' + str(cr) + '/' + str(maxx))
        bar.config(maximum=maxx)
        bar['value'] = cr
        bar.grid(row=0, column=0, sticky=W + E, padx=(5, 5), pady=(5, 5))
        lbl.grid(row=1, column=0, padx=(5, 5), pady=(5, 5))
        window1.update()
        if cr >= maxx:
            window1.destroy()

    if varr == '0': # лица
        faces()
    elif varr == '1': # текст
        text_recognition()