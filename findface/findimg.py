# AIGod by AIGod, GPL-3.0 license
import dlib
from skimage import io #scikit-image
from scipy.spatial import distance
import os
from tkinter import *
from tkinter.ttk import Progressbar
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
import shutil
from findface.findres import faceresult

dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))
pth = dir.replace('findface', '')

try:
    ff = open(pth + 'option.dat', 'r')
    oplist = ff.readlines()
    ff.close()
except IOError:
    oplist = ['en', 'cosmo']

list_res = []

def faces():
    global list_res
    list_res = []
    sp = dlib.shape_predictor(dir+"/shape_predictor_68_face_landmarks.dat") # модель обноружение черт лица
    facerec = dlib.face_recognition_model_v1(dir+"/dlib_face_recognition_resnet_model_v1.dat")
    detector = dlib.get_frontal_face_detector()

    face_descriptor1, face_descriptor2 = None, None

    rf = open(pth + 'optionmodel.dat', 'r')
    rrr = rf.readlines()
    u1 = rrr[3]
    cr1 = rrr[4]
    anal = rrr[11]
    rf.close()
    u1 = str(''.join(u1.split('\n')[:1]))
    cr1 = str(''.join(cr1.split('\n')[:1]))
    anal = str(''.join(anal.split('\n')[:1]))

    curafl, maxcfl = 0, 0
    arrRes = None
    pathRes = dir + '/esrgan/results/' # 'foto/'

    for dirs1,folder1,files1 in os.walk(pathRes):
        arrRes = files1
        maxcfl = len(arrRes)

    ff = open(pth + 'find.dat', 'r')
    olist = ff.readlines()
    ff.close()

    # проверка темы
    try:
        theme_stand = str(''.join(oplist[1].split('\n', 1)))
    except IOError:
        theme_stand = 'cosmo'
    window1 = ttk.Toplevel(title="AIGod", iconphoto=pth + "bin/image/logo.png",
                           resizable=[0, 0])
    ttk.Style().theme_use(theme_stand)
    # лейблы, инпуты, кнопки
    lbl = Label(window1, text='Comparison: 0/0')
    bar = Progressbar(window1, maximum=0)
    bar['value'] = 0
    bar.grid(row=0, column=0, sticky=W + E, padx=(5, 5), pady=(5, 5))
    window1.update()

    win2 = None
    win1 = None
    num = 0

    for i in arrRes:
        img1 = io.imread(pathRes + i) # путь к проверяемому фото
        win1 = dlib.image_window()
        win1.clear_overlay()
        dets1 = detector(img1, 1) # поиск лица на фото
        for k, d in enumerate(dets1):
            shape = sp(img1, d)
            win1.clear_overlay()
            win1.add_overlay(d)
            win1.add_overlay(shape)
            face_descriptor1 = facerec.compute_face_descriptor(img1, shape) # получение дескриптора лица

        arrFile = None
        curbl, maxbl = 0, 0

        rr = open(pth + 'analizy.dat', 'r')
        ph = rr.readlines()
        rr.close()
        ph[2] = str(''.join(ph[2].split('\n')[:1]))
        if ph[2] != '':
            path = ph[2]
        else:
            path = pth+'dataface/' # 'base/'

        for dirs2,folder2,files2 in os.walk(path):
            arrFile = files2
            maxbl = len(arrFile)
        for j in arrFile:
            img2 = io.imread(path+'/'+j) # путь к фото для сравнения
            win2 = dlib.image_window()
            win2.clear_overlay()
            dets2 = detector(img2, 1) # поиск лица на фото
            for k, d in enumerate(dets2):
                shape = sp(img2, d)
                win2.clear_overlay()
                win2.add_overlay(d)
                win2.add_overlay(shape)
                face_descriptor2 = facerec.compute_face_descriptor(img2, shape) # получение дескриптора лица
            curbl += 1
            curafl += 1

            if face_descriptor1 != None and face_descriptor2 != None:
                try:
                    a = distance.euclidean(face_descriptor1, face_descriptor2) # расчет разницы между двумя дескриптарами лиц (евклидово расстояние)
                except:
                    break

                if a<0.6:
                    curafl += maxbl - curbl

                curr = curafl
                maxx = maxcfl * maxbl
                lbl.config(text='Comparison: ' + str(curr) + '/' + str(maxx))
                # прогрессбар
                bar.config(maximum=maxx)
                bar['value'] = curr
                bar.grid(row=0, column=0, sticky=W + E, padx=(5, 5), pady=(5, 5))
                lbl.grid(row=1, column=0, padx=(5, 5), pady=(5, 5))
                window1.update()

                if a<0.6:
                    #print("Похожи")
                    a = (1.0-a)*100
                    a = float('{:.2f}'.format(a))
                    print(i+" == "+j+" -> "+str(a)+"%")

                    analife = False
                    if u1 == 'url' or u1 == 'web':
                        if cr1 == 'true' and anal == 'true':
                            analife = True

                    if analife == True:
                        try:
                            shutil.copy(pathRes + i, pth + 'findface/esrgan/objects/') # 2 вконце +i
                            list_res.append(pth + 'findface/esrgan/objects/' + i + '=====' + path + j)
                            wrs = open(pth + 'resfind.dat', 'a')
                            wrs.write(pth + 'findface/esrgan/objects/' + i + '=====' + path + j + '\n')
                            wrs.close()
                        except OSError:
                            pass
                    else:
                        list_res.append(pathRes + i + '=====' + path + j)

                        wrs = open(pth + 'resfind.dat', 'a')
                        wrs.write(pathRes + i + '=====' + path + j + '\n')
                        wrs.close()

                    num += 1
                    break
                else:
                    if curbl == maxbl:
                        print("Разные. Удалить: "+j+" == "+i)
                        olist[num] = str(''.join(olist[num].split('\n')[:1]))
                        try:
                            os.remove(pathRes+i) # удаление проверяемую фотографии
                            if u1 == 'url' or u1 == 'web':
                                if cr1 == 'true' and anal == 'true':
                                    try:
                                        os.remove(olist[num]) # удаление проверяемую фотографии оригинал
                                    except:
                                        pass
                        except OSError:
                            print('Error: ' + pathRes+i + '\nError: ' +  olist[num])
                        num += 1
                    else:
                        print("Разные: " + j +" == " + i)
                if curr >= maxx:
                    window1.destroy()
            else:
                curafl += -1 * (curbl - maxbl)

                curr = curafl
                maxx = maxcfl * maxbl
                lbl.config(text='Comparison: ' + str(curr) + '/' + str(maxx))
                # прогрессбар
                bar.config(maximum=maxx)
                bar['value'] = curr
                bar.grid(row=0, column=0, sticky=W + E, padx=(5, 5), pady=(5, 5))
                lbl.grid(row=1, column=0, padx=(5, 5), pady=(5, 5))
                window1.update()

                if face_descriptor1 == None:
                    print("Не корректное фото. Удалить: " + pathRes + i)
                    olist[num] = str(''.join(olist[num].split('\n')[:1]))
                    try:
                        os.remove(pathRes+i) # удаление проверяемую фотографии
                        if u1 == 'url' or u1 == 'web':
                            if cr1 == 'true' and anal == 'true':
                                try:
                                    os.remove(olist[num])  # удаление проверяемую фотографии оригинал
                                except:
                                    pass
                    except OSError:
                        print('Error: ' + pathRes+i + '\nError: ' + olist[num])
                    num += 1
                    break
                if face_descriptor2 == None:
                    print("Не корректное фото. Удалить: " + path + j)

                if curr >= maxx:
                    window1.destroy()
    win2 = None
    win1 = None
    if len(list_res) != 0:
        faceresult()
    else:
        wropt = open(pth + 'analizy.dat', 'a')
        wropt.write("false")
        wropt.close()