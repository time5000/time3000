# AIGod by AIGod, GPL-3.0 license
import os
import shutil
from tkinter import *
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
from PIL import Image, ImageTk

dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))
pth = dir.replace('findface', '')

list_res = []
imagess = []

def faceresult(): #fotocheck, fotobase
    global list_res, imagess
    list_res, imagess = [], []
    try:
        ff = open(pth + 'option.dat', 'r')
        oplist = ff.readlines()
        ff.close()
    except IOError:
        oplist = ['en', 'cosmo']

    class AutoScrollbar(Scrollbar):
        def set(self, low, high):
            if float(low) <= 0.0 and float(low) >= 1.0:
                self.grid_remove()
            else:
                self.grid()
                Scrollbar.set(self, low, low)

        def pack(self, **kw):
            raise TclError('Cannot use pack with the widget ')

        def place(self, **kw):
            raise TclError('Cannot use place with the widget ')

    def closes():
        dirs = pth + 'findface/esrgan/results/'
        try:
            for files in os.listdir(dirs):
                path = os.path.join(dirs, files)
                try:
                    shutil.rmtree(path)
                except OSError:
                    os.remove(path)
        except:
            pass

        try:
            os.remove(pth + 'find.dat')
        except OSError:
            print('Error: ' + pth + 'find.dat')

        try:
            os.remove(pth + 'resfind.dat')
        except OSError:
            print('Error: ' + pth + 'resfind.dat')
        window2.destroy()

    # проверка темы
    try:
        theme_stand = str(''.join(oplist[1].split('\n', 1)))
    except IOError:
        theme_stand = 'cosmo'
    window2 = ttk.Toplevel(title="AIGod", iconphoto=pth + "bin/image/logo.png", size=[400, 200],
                           resizable=[0, 0])
    ttk.Style().theme_use(theme_stand)

    hbar = AutoScrollbar(window2, orient='horizontal')
    vbar = AutoScrollbar(window2, orient='vertical')
    vbar.grid(row=0, column=1, sticky='ns')
    hbar.grid(row=1, column=0, sticky='we')
    canvas = Canvas(window2, highlightthickness=0, xscrollcommand=hbar.set, yscrollcommand=vbar.set)
    canvas.grid(row=0, column=0, sticky='nswe')
    canvas.update()
    hbar.configure(command=canvas.xview)
    vbar.configure(command=canvas.yview)
    window2.grid_rowconfigure(0, weight=1)
    window2.grid_columnconfigure(0, weight=1)
    frame = Frame(canvas)
    frame.rowconfigure(1, weight=1)
    frame.columnconfigure(1, weight=1)

    spi = open(pth + 'resfind.dat', 'r')
    ttt = spi.readlines()
    spi.close()

    for fls in ttt:
        fls = str(''.join(fls.split('\n')[:1]))
        list_res.append(fls)

    # разделить схожие фото по спискам
    listing1 = []
    listing2 = []
    for xxx in list_res:
        listing1.append(str(''.join(xxx.split('=====', 1)[1])))
        listing2.append(str(''.join(xxx.split('=====')[0])))

    # имена переменных для новых виджетов
    nameswid1 = []
    for jj1 in range(len(listing2)):
        nameswid1.append('nameWid1'+str(jj1))
    nameswid2 = []
    for jj2 in range(len(listing1)):
        nameswid2.append('nameWid2' + str(jj2))
    nameswid3 = []
    for jj3 in range(len(listing1)):
        nameswid3.append('nameWid3' + str(jj3))

    # добавление довых виджетов
    j, k = 0, 0
    for ii in range(len(list_res)):
        # лейблы, инпуты, кнопки
        imgg1 = Image.open(listing2[ii])
        wpercent1 = (150 / float(imgg1.size[1]))
        wsize1 = int((float(imgg1.size[0]) * float(wpercent1)))
        filename1 = ImageTk.PhotoImage(imgg1.resize((wsize1, 150)))
        imagess.append(filename1)
        nameswid1[ii] = Label(frame, image=imagess[ii+j]).grid(row=ii+k, column=0, padx=(5, 5), pady=(5, 5))
        j += 1
        imgg2 = Image.open(listing1[ii])
        wpercent2 = (150 / float(imgg2.size[1]))
        wsize2 = int((float(imgg2.size[0]) * float(wpercent2)))
        filename2 = ImageTk.PhotoImage(imgg2.resize((wsize2, 150)))
        imagess.append(filename2)
        nameswid2[ii] = Label(frame, image=imagess[ii+j]).grid(row=ii+k, column=1, padx=(5, 5), pady=(5, 5))
        name = str(''.join(listing1[ii].split("/")[-1]))
        nameswid3[ii] = Label(frame, text=name).grid(row=ii+k+1, column=1, padx=(5, 5), pady=(5, 5))
        k += 1
        window2.update()

    canvas.create_window(0, 0, anchor=NW, window=frame)
    frame.update_idletasks()
    canvas.config(scrollregion=canvas.bbox("all"))

    # Закрытие окна
    window2.protocol('WM_DELETE_WINDOW', closes)

    window2.update()