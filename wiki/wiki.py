import wikipedia
import os
from translate import Translator
from tkinter import *
from tkinter.ttk import Progressbar
import ttkbootstrap as ttk
from ttkbootstrap.constants import *
import random

dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))
pth = dir.replace('wiki', '')

def searh_2loop(wiki_page2):
    try:
        txt = str(wiki_page2.summary)
        pathss = open(pth + 'objectsText.dat', 'a')
        pathss.write(txt+'\n###############################################\n\n')
        pathss.close()
    except wikipedia.exceptions.DisambiguationError as e:
        pass
    except wikipedia.exceptions.PageError:
        pass

tra = ''

def switch(lang_text):
    try:
        ff = open(pth + 'option.dat', 'r')
        oplist = ff.readlines()
        ff.close()
    except IOError:
        oplist = ['en', 'cosmo']

    # проверка темы
    try:
        theme_stand = str(''.join(oplist[1].split('\n', 1)))
    except IOError:
        theme_stand = 'cosmo'
    window1 = ttk.Toplevel(title="AIGod", iconphoto=pth + "bin/image/logo.png",
                           resizable=[0, 0])
    ttk.Style().theme_use(theme_stand)
    # лейблы, инпуты, кнопки
    lbl = Label(window1, text='Comparison: 0/0')
    bar = Progressbar(window1, maximum=0)
    bar['value'] = 0
    bar.grid(row=0, column=0, sticky=W + E, padx=(5, 5), pady=(5, 5))
    window1.update()


    rr = open(pth + 'objects.dat', 'r')
    ph = rr.readlines()
    rr.close()

    global tra
    curr, maxx = 0, 0

    if ph != '':
        maxx = len(ph)
        bar.config(maximum=maxx)
        for wiki_text2 in ph:
            wiki_text2 = str(''.join(wiki_text2.split('\n')[:1]))

            if lang_text == 'en': # english
                wikipedia.set_lang("en")
                tra = wiki_text2
            elif lang_text == 'sp': # spanish
                wikipedia.set_lang("es")
                trans(wiki_text2, 'es')
            elif lang_text == 'fr': # french
                wikipedia.set_lang("fr")
                trans(wiki_text2, 'fr')
            elif lang_text == 'ua': # ukraine
                wikipedia.set_lang("uk")
                trans(wiki_text2, 'uk')
            elif lang_text == 'gr': # german
                wikipedia.set_lang("de")
                trans(wiki_text2, 'de')
            elif lang_text == 'it': # italian
                wikipedia.set_lang("it")
                trans(wiki_text2, 'it')
            elif lang_text == 'ru': # russian
                wikipedia.set_lang("ru")
                trans(wiki_text2, 'ru')
            elif lang_text == 'nr': # norwegian
                wikipedia.set_lang("en") #google - no
                tra = wiki_text2

            try:
                wiki_page1 = wikipedia.page(tra, auto_suggest=False)
            except wikipedia.DisambiguationError as e:
                s = random.choice(e.options)
                wiki_page1 = wikipedia.page(s)
            searh_2loop(wiki_page1)

            curr += 1
            lbl.config(text='Comparison: ' + str(curr) + '/' + str(maxx))
            bar['value'] = curr
            bar.grid(row=0, column=0, sticky=W + E, padx=(5, 5), pady=(5, 5))
            lbl.grid(row=1, column=0, padx=(5, 5), pady=(5, 5))
            window1.update()
            if curr >= maxx:
                window1.destroy()
    else:
        pathss = open(pth + 'objectsText.dat', 'a')
        pathss.write('No text' + '\n###############################################\n\n')
        pathss.close()
        window1.destroy()

def trans(stexts, lng):
    global tra
    translator = Translator(from_lang="en", to_lang=lng)
    translation = translator.translate(stexts)
    tra = translation